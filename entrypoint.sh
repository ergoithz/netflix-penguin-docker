#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -x

useradd --create-home --uid ${USER_UID} --group ${USER_GID} "${USER_NAME}"
HOME="/home/${USER_NAME}"

mkdir -p /netflix-penguin-cache "${HOME}/.cache"
chown "${USER_UID}:${USER_GID}" /netflix-penguin-cache "${HOME}"
ln -s /netflix-penguin-cache "${HOME}/.cache/netflix-penguin"

# reach the interactive shell even when things go wrong
set +o errexit

sudo --set-home --preserve-env -u "${USER_NAME}" env \
    WINE=/opt/wine-staging/bin/wine \
    WINEPREFIX="/home/${USER_UID}/.wine-pipelight" \
    WINEARCH=win32 \
    QUIETINSTALLATION=1 \
    /usr/share/pipelight/install-dependency \
    wine-silverlight5.1-installer wine-mpg2splt-installer

pipelight-plugin --create-mozilla-plugins
sudo --set-home --preserve-env -u "${USER_NAME}" netflix-penguin

test -t 0 && test -t 1 && {
    set +x
    exec bash -l
}
