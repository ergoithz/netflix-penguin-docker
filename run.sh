#!/bin/bash -e

IMAGE=penguin:latest
USER_UID=`id -u`
USER_GID=`id -g`

CACHEDIR="$HOME/.cache/netflix-penguin"

mkdir -p "$CACHEDIR"

set -x
$DOCKER_REQUIRE_SUDO docker run --rm -it --privileged \
  $USER_VOLUME \
  $(find /dev/dri -type c -printf '--device=%p ') \
  --env DISPLAY="$DISPLAY" \
  --env USER_NAME="$USER" \
  --env USER_UID=$USER_UID \
  --env USER_GID=$USER_GID \
  --env PIPELIGHT_GPUACCELERATION=0 \
  --workdir "$HOME" \
  --volume "/tmp/.X11-unix:/tmp/.X11-unix:ro" \
  --volume "/run/user/$USER_UID/pulse:/run/pulse:ro" \
  --volume "$CACHEDIR:/netflix-penguin-cache" \
  $IMAGE
