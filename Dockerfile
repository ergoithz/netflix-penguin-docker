FROM ubuntu:16.04
MAINTAINER Felipe A. Hernandez <ergoithz@gmail.com>

ENTRYPOINT /entrypoint.sh
ENV PULSE_SERVER /run/pulse/native

RUN set -xe \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y update \
    && apt-get install -y software-properties-common \
    && add-apt-repository -y ppa:gnome3-team/gnome3-staging \
    && add-apt-repository -y ppa:pipelight/stable \
    && apt-add-repository -y multiverse \
    && apt-get -y update \
    && apt-get install -y --install-recommends \
      libpulse0 \
      pulseaudio \
      python3-pip \
      python3-gi \
      gir1.2-gtk-3.0 \
      gir1.2-webkit2-4.0 \
      sudo \
    && echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula boolean true | debconf-set-selections \
    && dpkg --add-architecture i386 \
    && apt-get -y update \
    && apt-get install -y --install-recommends \
      pipelight-multi \
    && pipelight-plugin --update \
    && pipelight-plugin --accept --enable silverlight \
    && echo enable-shm=no >> /etc/pulse/client.conf \
    && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /entrypoint.sh
ADD https://github.com/ergoithz/netflix-penguin/archive/0.0.3.zip /tmp/netflix-penguin.tar.gz

RUN set -xe \
    && pip3 install /tmp/netflix-penguin.tar.gz \
    && rm -rf /tmp/netflix-penguin.tar.gz
